from django.shortcuts import render, HttpResponse
from .models import *
from collections import defaultdict

# Create your views here.
def index(request):
    #----------------------------
    # Mismo Grupo 1: Ejemplo del ayudante 
    grupo1 = Estudiante.objects.filter(grupo=1)
    #print(grupo1)

    #----------------------------
    # Mismo grupo 4  
    grupo4 = Estudiante.objects.filter(grupo=4)
    
    #----------------------------
    # Mismo apellido
    mismoApellido1 =Estudiante.objects.filter(apellidos="Mendoza")
    mismoApellido2 =Estudiante.objects.filter(apellidos="Landaverde")
    mismoApellido3 =Estudiante.objects.filter(apellidos="Garces")
    mismoApellido4 =Estudiante.objects.filter(apellidos="Gómez")
    
    #----------------------------
    # Misma edad
    # Obtener todas las edades de estudiantes en la base de datos
    edades = Estudiante.objects.values_list('edad', flat=True)
   
    # Crear un diccionario que almacena estudiantes con la misma edad
    estudiantes_por_edad = defaultdict(list)
    for estudiante in Estudiante.objects.all():
        estudiantes_por_edad[estudiante.edad].append(estudiante.nombres)

    # Crear una lista de edades repetidas y asociarlas a los nombres de estudiantes que tengan la misma edad
    edadesRepetidas = []
    for edad, estudiantes in estudiantes_por_edad.items():
        if len(estudiantes) > 1:
            edadesRepetidas.append({'edad': edad, 'estudiantes': estudiantes})
    
    #----------------------------
    # Grupo 3 y misma edad.
    # Crear un diccionario que almacena estudiantes con la misma edad y que estén en el grupo 3
    estudiantes_por_edad_grupo3 = defaultdict(list)
    for estudiante in Estudiante.objects.filter(grupo=3):
        estudiantes_por_edad_grupo3[estudiante.edad].append(estudiante.nombres+" "+estudiante.apellidos)
    
    # Crear una lista de edades repetidas y asociarlas a los nombres de estudiantes que tengan la misma edad y estén en el grupo 3
    edadesRepetidas_grupo3 = []
    for edad, estudiantes in estudiantes_por_edad_grupo3.items():
        if len(estudiantes) > 1:
            edadesRepetidas_grupo3.append({'edad': edad, 'estudiantes': estudiantes})
    #----------------------------
    # Todos los estudiantes.
    todos = Estudiante.objects.all()

    return render (request,'index.html',{'grupo1':grupo1,'grupo4':grupo4,'mismoApellido1': mismoApellido1,'mismoApellido2': mismoApellido2,'mismoApellido3': mismoApellido3,'mismoApellido4': mismoApellido4,'edadesRepetidas':edadesRepetidas,'edadesRepetidas_grupo3':edadesRepetidas_grupo3,'todos':todos})
